export const Constants = {
  apiURL: 'https://rickandmortyapi.com/api',
  colors: {
    bgColor: '#24282F',
    textColor: 'white',
    itemColor: '#3C3E43',
  },
};
