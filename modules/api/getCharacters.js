import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import * as FileSystem from 'expo-file-system';
import { Platform } from 'react-native';
import { Constants } from '../constants/constants';

const imageDir = FileSystem.cacheDirectory + 'image/';

export default async function getCharacters(next) {
  const url = next || `${Constants.apiURL}/character`;
  let data = null;
  try {
    const result = await axios.get(url);
    const characters = [];
    for (let character of result.data.results) {
      if (Platform.OS === 'native') {
        const base64Image = await saveImage(character);
        characters.push({ ...character, image: base64Image });
      } else {
        characters.push(character);
      }
    }
    result.data.results = characters;
    await AsyncStorage.setItem(url, JSON.stringify(result.data));
    data = result.data;
  } catch (e) {
    const cachedResult = await AsyncStorage.getItem(url);
    if (cachedResult) {
      data = JSON.parse(cachedResult);
    }
    console.error(e);
  }
  return data;
}

async function saveImage(character) {
  await ensureDirExists();
  const fileUri = `${imageDir}${character.id}`;
  const fileInfo = await FileSystem.getInfoAsync(fileUri);
  if (!fileInfo.exists) {
    await FileSystem.downloadAsync(character.image, fileUri);
  }
  return fileUri;
}

async function ensureDirExists() {
  const dirInfo = await FileSystem.getInfoAsync(imageDir);
  if (!dirInfo.exists) {
    await FileSystem.makeDirectoryAsync(imageDir, { intermediates: true });
  }
}
