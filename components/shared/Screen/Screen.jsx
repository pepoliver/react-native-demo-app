import React from 'react';
import { SafeAreaView } from 'react-native';
import { styles } from './Screen.styles';

export default function Screen({ children }) {
  return <SafeAreaView style={styles.container}>{children}</SafeAreaView>;
}
