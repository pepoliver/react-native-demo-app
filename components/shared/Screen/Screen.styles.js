import { StyleSheet } from 'react-native';
import { Constants } from '../../../modules/constants/constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constants.colors.bgColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
