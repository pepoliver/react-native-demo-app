import React from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Screen from '../../shared/Screen/Screen';
import useHomeHook from './Home.hook';
import { styles } from './Home.styles';

export default function Home() {
  const { state, actions } = useHomeHook();
  return (
    <Screen>
      <FlatList
        style={styles.list}
        data={state.characters}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={styles.item}
            key={item.id}
            onPress={() => {
              actions.goToDetail(item.id);
            }}
          >
            <Image style={styles.image} source={{ uri: item.image }} />
            <Text style={styles.name}>{item.name}</Text>
          </TouchableOpacity>
        )}
        keyExtractor={(item) => item.id.toString()}
        onEndReached={actions.loadNext}
        onEndReachedThreshold={0.1}
      ></FlatList>
      {state.isLoading ? <ActivityIndicator /> : null}
    </Screen>
  );
}
