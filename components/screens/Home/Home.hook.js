import { useEffect, useState, useContext } from 'react';
import { useNavigation } from '@react-navigation/native';
import getCharacters from '../../../modules/api/getCharacters';
import { AppContext } from '../../../App.context';

export default function useHomeHook() {
  const navigation = useNavigation();
  const { state: appState, actions: appActions } = useContext(AppContext);
  const [state, setState] = useState({
    next: null,
    isLoading: true,
  });
  const updateState = (newState) => {
    setState((previousState) => ({ ...previousState, ...newState }));
  };

  useEffect(() => {
    loadCharacters(state, updateState, appState, appActions);
  }, []);

  const actions = {
    loadNext: async () => {
      if (state.next) {
        updateState({
          isLoading: true,
        });
        try {
          await loadCharacters(state, updateState, appState, appActions);
        } catch (e) {
          updateState({
            isLoading: false,
          });
        }
      }
    },
    goToDetail: (characterId) => {
      navigation.navigate('Detail', { characterId });
    },
  };

  return { state: { ...state, characters: appState.characters }, actions };
}

async function loadCharacters(state, updateState, appState, appActions) {
  const newCharacters = await getCharacters(state.next);
  if (newCharacters?.results) {
    const characters = [...appState.characters, ...newCharacters.results];
    updateState({
      next: newCharacters.info.next,
      isLoading: false,
    });
    appActions.saveCharacters(characters);
  }
}
