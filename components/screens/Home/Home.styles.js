import { StyleSheet } from 'react-native';
import { Constants } from '../../../modules/constants/constants';

export const styles = StyleSheet.create({
  list: {
    width: '100%',
  },
  item: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    backgroundColor: Constants.colors.itemColor,
    margin: 10,
  },
  name: {
    fontSize: 20,
    color: Constants.colors.textColor,
  },
  image: {
    width: 60,
    height: 60,
    marginRight: 20,
  },
});
