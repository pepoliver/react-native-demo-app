import React, { useContext } from 'react';
import { Image, Text, View } from 'react-native';
import Screen from '../../shared/Screen/Screen';
import { styles } from './Detail.styles';
import { useRoute } from '@react-navigation/native';
import { AppContext } from '../../../App.context';

export default function Detail({}) {
  const { state: appState } = useContext(AppContext);
  const route = useRoute();
  const character = appState.characters.find(
    (character) => character.id == route.params.characterId
  );
  return (
    <Screen>
      {character && (
        <View style={styles.header}>
          <Image style={styles.image} source={{ uri: character.image }} />
          <View>
            <Text style={styles.name}>{character.name}</Text>
            <Text style={styles.text}>{character.species}</Text>
            <Text style={styles.text}>{character.status}</Text>
          </View>
        </View>
      )}
    </Screen>
  );
}
