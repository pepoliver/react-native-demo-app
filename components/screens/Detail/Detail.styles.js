import { StyleSheet } from 'react-native';
import { Constants } from '../../../modules/constants/constants';

export const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  name: {
    fontSize: 20,
    color: Constants.colors.textColor,
  },
  text: {
    color: Constants.colors.textColor,
    marginTop: 10,
    fontSize: 16,
  },
  image: {
    width: 120,
    height: 120,
    marginRight: 20,
  },
});
