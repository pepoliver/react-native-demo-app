import React from 'react';
import AppContextProvider from './App.context';
import AppStackNavigator from './components/navigation/AppStackNavigator';

export default function App() {
  return (
    <AppContextProvider>
      <AppStackNavigator />
    </AppContextProvider>
  );
}
