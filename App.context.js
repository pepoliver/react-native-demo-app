import React, { useState } from 'react';

export const AppContext = React.createContext();

export default function AppContextProvider({ children }) {
  const [state, setState] = useState({
    characters: [],
  });

  const actions = {
    saveCharacters: async (characters) => {
      setState({ ...state, characters });
    },
  };

  return (
    <AppContext.Provider value={{ state, actions }}>
      {children}
    </AppContext.Provider>
  );
}
